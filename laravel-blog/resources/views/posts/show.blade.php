@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <!-- Post Details -->
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            <!-- Display Number of Likes and Comments -->
            <div class="mt-3 text-muted">
                <div class="d-flex justify-content-between align-items-center">
                    <p class="mb-0">Likes: {{ $post->likes->count() }} | Comments: {{ $post->comments->count() }}</p>
                </div>
            </div>

            <!-- Like/Unlike Form -->
            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
                Add Comment
            </button>

            <div class="mt-3">
                <a href="/posts" class="card-link">View all posts</a>
            </div>
        </div>
    </div>

    <!-- Comment Modal -->
    <div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="commentModalLabel">Add a Comment</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('posts.comment', ['id' => $post->id]) }}">
                        @csrf
                        <div class="mb-3">
                            <label for="content" class="form-label">Comment Content</label>
                            <textarea class="form-control" id="content" name="content" rows="4" required></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit Comment</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Comments Section -->
    <div class="mt-3">
        <h3>Comments</h3>
        @foreach ($post->comments as $comment)
            <div class="card mb-3">
                <div class="card-body">
                    <h2 class="card-title text-center">{{ $comment->content }}</h2>
                    <p class="card-text text-muted text-center">Posted by: {{ $comment->user->name }}</p>
                    <p class="card-text text-muted text-center">Posted on: {{ $comment->created_at }}</p>
                </div>
            </div>
        @endforeach
    </div>


@endsection
