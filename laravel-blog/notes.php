====================SESSION 01====================

[SECTION] MVC Architecture

- Laravel is scalable and works with minimal configuration because it follows the "Model-View-Controller" architecture.

- "Model" component corresponds to all the data-related logic.

- "View" component is used for all the UI logic of the application.

- "Controllers" component act as an interface between Model and View components to process all the business logic and incoming requests, manipulate data using the Model component and interact with the Views to render the final output.


[SECTION] Create a laravel project

Syntax:

	composer create-project laravel/laravel <project-name>

After installation we can test if the application is running with the command:

	php artisan serve

	Note: Make sure that Apache and MySQL is started in the XAMPP.

[SECTION] Folder Structure

app folder
	- contains the models at its root. Models represent our database entities and have pre-defined methods for querying the respective tables that they represent.

	- "Http" -> Controllers subdirectory contains the project's controllers where we define our application/business logic.

database folder
	- "migrations subdirectory" contains the migrations that we will use to define the structures and data types of our database tables.

public folder
	- views subdirectory is the namespace where all views will be looked for by our application.

resources folder
	- where assets such as css, js, images, etc. can be stored and accessed.

routes folder
	- "web.php" file is where we define the routes of our web application.

.env file at the root of our project directory is where we set our application settings including database connection settings


[SECTION] Generate the authentication scaffolding

- Auth Scaffolding automatically creates a user registration, login, dashboard, logout, reset password and email verification with the help of Laravel UI package.

- install Laravel's laravel/ui package via the terminal command:

	composer require laravel/ui

- then build the authentication scaffolding via the terminal command (This wil create the login & register page):

	php artisan ui bootstrap --auth

- compile our fresh scaffolding with the command:

	npm install && npm run dev

	Note: npm run dev should be running when accessing the login and registration field.

[SECTION] Generating Model Classes with its corresponding migrations and controllers

	Syntax:

		php artisan make:model <ModelName> -mc

		- Model created is Eloquent Model(Eloquent is Laravel's ORM - Object Relational Model)
			- This will be used by laravel to map its relational database table.
			- Eloquent models have pre-defined methods database queries and operations.

		- Adding the "-mc" will also create the migrations and controller of the model
		- Migrations (m) are like version control for your database, allowing your team to define and share the application's database schema definition..
			- A migration class contains two methods:
				- "up" method is used to add new tables, columns, or indexes to your database
				- "down" method should reverse the operations performed by the up method.

		- controller (c) are meant to group associated request handling logic within a single class.

		- We can also create this component individually:
		 - php artisan make:migration <create_migration_name_table>

		 - php artisan make:controller <ControllerName>
		 	
[SECTION] Migration, Model, and Controllers

	- Migration will contain the schema of the entity and allows you to create the table in your database. It also allows you to modify the created tables

	- Model will be use for setting up the relationship between Models.

	- Controllers will contain the business logic and database query.

[SECTION] Running Migrations

If the targeted database is currently empty/a new table is created, we can run our migrations via the terminal command:

	php artisan migrate

Otherwise, if table already exists and we have modify the table columns:

	php artisan migrate:fresh

	Note: This will drop the existing table first before migration. So the data existing in the database will be removed.

====================SESSION 02====================

[SECTION] Flow of Routes, Controller, Views

	- Routes (routes folder->web.php)
	- Controller (app folder -> Http folders -> Controllers folders -> controller_name.php)
	- Views (views folder)

[SECTION] Defining Routes in in web.php

Code snippet:

	Route::get('/posts/create', [PostController::class, 'create']);

	Code Breakdown:
		- "Route::get('/posts/create', ...)": defines a route for HTTP GET requests to the URL path.
		- "[PostController::class, 'create']" specifies the controller method that should be invoked when this route is accessed.

Action handled by Resource Controllers

	https://laravel.com/docs/10.x/controllers#actions-handled-by-resource-controller

	Here are the typical resource controller actions and their corresponding HTTP verbs and URLs:

	1. Index: Lists all resources
		HTTP Verb: GET
		URL: /resource

	2. Create: Displays a form to create a new resource
		HTTP Verb: GET
		URL: /resource/create

	3. Store: Saves a new resource to the database
		HTTP Verb: POST
		URL: /resource

	4. Show: Displays a specific resource
		HTTP Verb: GET
		URL: /resource/{id}

	5. Edit: Displays a form to edit an existing resource
		HTTP Verb: GET
		URL: /resource/{id}/edit

	6. Update: Updates an existing resource in the database
		HTTP Verb: PUT/PATCH
		URL: /resource/{id}

	7. Destroy: Deletes a specific resource
		HTTP Verb: DELETE
		URL: /resource/{id}

Scope resolution operator or "::"
- is used to access static methods, static properties, and constants of a class without needing to create an instance of that class.
-  It allows you to work with classes and their members in a clean and organized manner, especially in the context of Laravel's expressive syntax.

[SECTION] Defining a controller action (views)
	- A controller is in form of class and contains different methods that performs actions.
	- Example:
		//action to return a view containing a form for blog post creation
    public function create()
    {
        return view('posts.create');
    }

			Notes:
				- view() method is used to render a specific view in the browser.
					- This is a key part of how Laravel handles the presentation layer of your application.
				- Views are typically stored in the 'resources/views' directory, and their filenames correspond to the view name you specify in the controller.
				-  Blade (.blade.php) is Laravel's templating engine, which allows you to embed PHP code and provides additional features like template inheritance, conditional statements, loops, and more.

[SECTION] Creating a view file using laravel's templating engine

Code snippet:
	@extends('layouts.app')

	@section('content')
	    <form method="POST" action="/posts">
	        @csrf
	      <div class="form-group">
	        <label for="title">Title:</label>
	        <input type="text" class="form-control" id="title" name="title">
	      </div>
	      <div class="form-group">
	        <label for="content">Content:</label>
	        <textarea class="form-control" id="content" name="content" rows="3"></textarea>
	      </div>
	      <div class="mt-2">
	          <button type="submit" class="btn btn-primary">Create Post</button>
	        </div>
	    </form>
	@endsection

Code Breakdown:
	-  "@" symbol is used to indicate the start of a Blade directive.
		- Blade directives are special instructions or control structures that allow you to embed PHP code and perform various operations within your Blade templates.
	- "@extends": By using this directive, you can include this common structure in your master layout/view.
		- A "master layout" contains the common structure of your web pages, such as headers, footers, navigation menus, and more.
	-"@section('content')...@endsection": This directive in Laravel Blade is used to define a named section in your Blade view. This named section serves as a placeholder within your view file where you can insert content.
		- The 'content' section name is typically defined in the master layout ('layouts.app').
		- This can be displayed using "@yield", which is used to define a placeholder in a layout file (usually referred to as a master layout) where content from child views can be injected.
	- "@csrf":This is a directive in Laravel Blade templates is used to generate a hidden input field that includes a CSRF token in your HTML forms.
		- CSRF stands for Cross-Site Request Forgery. It is a form of attack where malicious users may send malicious requests while pretending to be the authorized user.
		-  Laravel uses tokens to detect if form input requests have not been tampered with. Tampered with requests are not processed due to a likelihood that they were compromised via CSRF.

[SECTION] Creating a route and logic for saving post

	- Upon form submission, a "/posts" rout will handle the request.
	- Code snippet in web.php

		//define a route wherein form data will be sent via POST method to the /posts URI endpoint
		Route::post('/posts', [PostController::class, 'store']);

	- Within the PostController class definition, add the following method:

		//action to receive form data and subsequently store said data in the posts table
		//this action needs a parameter of class Request
		// The instantiation of Laravel's Request class contains data from the form submission.
		public function store(Request $request)
		{
		    //if there is an authenticated user
		    if(Auth::user()){
		        //instantiate a new Post object from the Post model
		        $post = new Post;
		        //define the properties of the $post object using the received form data
		        $post->title = $request->input('title');
		        $post->content = $request->input('content');
		        //get the id of the authenticated user and set it as the foreign key user_id of the new post
		        $post->user_id = (Auth::user()->id);
		        //save this post object in the database
		        $post->save();

		        return redirect('/posts');
		    }else{
		        return redirect('/login');
		    }
		}

[SECTION] Other notes

	- Laravel's Eloquent ORM:
		https://laravel.com/docs/10.x/queries
	- Method spoofing:
		- HTML do not support "PUT", "PATCH", or "DELETE"
    - using the @method() this will add a hidden _method field to a form and  will be used as the HTTP request method.