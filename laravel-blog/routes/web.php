<?php

use Illuminate\Support\Facades\Route;
// link the PostController Class
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// define a route wherein to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return  a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// s02 activity
// define a route that will return a view for the welcome page
Route::get('/', [PostController::class, 'welcome']);

// define a route wherein a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific post with matching URL parameter 'id' will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// Activity s03
// define a route that will return an edit form for a specific Post when a GET request is received at the /posts/{id}/edit endpoint.
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// Define a route that will overwrite an existing post with matching URL parameter via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// Define a route that will delete a post of matching URL parameter ID
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// Define a route that will archive a post of matching URL parameter ID
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// Define a route that will call the like action when a PUT request is received at the /posts/{id}/like
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// Define a route that will call the comment action when a POST request is received at the /posts/{id}/comment endpoint
Route::post('/posts/{id}/comment', [PostController::class, 'comment'])->name('posts.comment');

Route::get('/posts/{id}', [PostController::class, 'show'])->name('posts.show');
